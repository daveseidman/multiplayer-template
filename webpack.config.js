const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './client/index.js',
  devtool: 'source-map',
  devServer: {
    hot: true,
    https: true,
    host: '0.0.0.0',
    // TODO: use prefix (api/createRoom, api/joinRoom, etc) so we don't have to define each route
    proxy: {
      '/socket.io': { target: 'https://0.0.0.0:8000', secure: false, ws: true },
      '/playerName': { target: 'https://0.0.0.0:8000', secure: false },
      '/getRooms': { target: 'https://0.0.0.0:8000', secure: false },
      '/getPlayers': { target: 'https://0.0.0.0:8000', secure: false },
      '/createRoom': { target: 'https://0.0.0.0:8000', secure: false },
      '/joinRoom': { target: 'https://0.0.0.0:8000', secure: false },
      '/leaveRoom': { target: 'https://0.0.0.0:8000', secure: false },
    },
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({ template: 'client/index-template.html' }),
    new CopyWebpackPlugin([{ from: 'client/assets', to: 'assets' }]),
  ],
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        { loader: 'sass-loader' },
      ],
    }, {
      test: /\.(png|svg|jpg|gif|ttf|eot|woff|woff2|mp4|mtl|obj|fbx|gltf)$/,
      use: [
        'file-loader',
      ],
    }],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve('dist'),
  },
  node: {
    fs: 'empty',
  },
};
