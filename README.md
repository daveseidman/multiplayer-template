# Multiplayer Game Template

## Overview

Vanilla JavaScript, no external frameworks, minimal dependencies.

User Flow:
- Start: Capture users name and register it with a socket on the server
- Lobby: Join or create a room
- Game: Play a game against others in the same room


## Development

Start by creating a sym link from .env.development to .env `ln -s .env.development .env` we'll have one slight different on local vs production:

On your localhost set `SSL=false` inside of .env. This will inform the node server to use a self signed cert. The front end will be available at http*s*://localhost:8080 but chrome will warn you that the certificate isn't valid. This is fine for development.  

On the live server set `SSL=true` since it will run behind apache with a valid SSL.  


`npm run client-dev` starts webpack dev server on https://localhost:8080  
`npm run server-dev` starts express server with socket.io on http://localhost:8000  


This template includes almost no game logic aside from some fake controls that allow you to see how socket messages are passed from client to server and back to client.  

## Deploying

`npm run deploy` will build the site into a /dist folder using webpack and use rsync to move all files up to the server specified in package.json.

`npm run connect` will ssh you into that server. You'll need to first log in with the .pem key (not in repo), and then add your public key to the server's `~/.ssh/authorized_keys` file.


## Notes

The site is currently in a testing environment at https://multi-player.us  
This server should eventually be split out to allow for multiple apps

## TODO
- inspect how onChange effects io, we shouldn't have to pass it separately to game.js
