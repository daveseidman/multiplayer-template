require('dotenv').config();

const express = require('express');
const fs = require('fs');
const http = require('http');
const https = require('https');

const app = express();

const rooms = {};
const players = {};

const server = process.env.SSL === 'true'
  ? http.createServer(app)
  : https.createServer({ key: fs.readFileSync('server/server.key'), cert: fs.readFileSync('server/server.cert') }, app);

const io = require('socket.io').listen(server);

server.listen(8000, () => { console.log(`multiplayer server listening on http${process.env.SSL === 'true' ? '' : 's'}://localhost:8000`); });

const randomID = () => [...Array(4)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join('');

app.use('/', express.static(`${__dirname}/../dist`)); // TODO: can probably use a wildcard here

app.get('/playerName/:socketID/:name', (req, res) => {
  const { socketID, name } = req.params;
  console.log(`attaching name ${name} to socket ${socketID}`);
  io.sockets.sockets[socketID].name = name;
  players[socketID] = { name, socketID };
  res.send({ nameSet: true });
});


app.get('/createRoom', (req, res) => {
  let roomID = randomID();
  while (Object.keys(rooms).indexOf(roomID) >= 0) roomID = randomID();
  console.log('creating room', roomID);
  rooms[roomID] = {
    roomID,
    players: [],
    status: 'waiting',
  };
  io.emit('roomsUpdated', rooms);
  res.send({ roomID }); // TODO: not listening for this
});

app.get('/joinRoom/:socketID/:roomID', (req, res) => {
  const { socketID, roomID } = req.params;
  console.log(`${players[socketID].name} has joined room ${roomID}`);
  io.sockets.sockets[socketID].join(roomID);
  rooms[roomID].players.push(players[socketID]);
  io.to(roomID).emit('playersUpdated', rooms[roomID].players);
  res.send({ joined: true });
});

app.get('/leaveRoom/:socketID/:roomID', (req, res) => {
  const { socketID, roomID } = req.params;
  io.sockets.sockets[socketID].leave(roomID);
  rooms[roomID].players.forEach((player, index) => {
    if (player.socketID === socketID) {
      console.log(`${player.name} has left room ${roomID}`);
      rooms[roomID].players.splice(index, 1);
      io.to(roomID).emit('playersUpdated', rooms[roomID].players);
    }
  });
  // console.log(io.sockets.adapter.rooms);
  res.send({ left: true });
});

app.get('/getRooms', (req, res) => {
  res.send(rooms);
});

app.get('/getPlayers/:roomID', (req, res) => {
  const { roomID } = req.params;
  res.send(rooms[roomID].players);
});

io.on('connection', (socket) => {
  socket.on('direction', (data) => {
    io.to(Object.keys(socket.rooms)[1]).emit('direction', { player: players[socket.id].name, direction: data });
  });

  socket.on('disconnect', () => {
    Object.keys(rooms).forEach((roomID) => {
      rooms[roomID].players.forEach((player, index) => {
        if (player.socketID === socket.id) {
          console.log(player.name, 'has left');
          rooms[roomID].players.splice(index, 1);
          io.to(roomID).emit('playersUpdated', rooms[roomID].players);
        }
      });
    });
  });
});
