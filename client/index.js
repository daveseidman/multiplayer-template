import './index.scss';
import { addEl, createEl } from 'lmnt';
import onChange from 'on-change';
import autoBind from 'auto-bind';
import io from 'socket.io-client';
import Start from './modules/views/start';
import Lobby from './modules/views/lobby';
import Game from './modules/views/game';


class App {
  constructor() {
    autoBind(this);

    const state = {
      view: null,
      name: null,
      room: null,
      // socket: io(),
      players: [],
      cards: [],
    };
    this.state = onChange(state, this.update);

    const socket = io();

    this.el = createEl('div', { className: 'app' });

    this.views = {
      start: new Start(this.state, socket),
      lobby: new Lobby(this.state, socket),
      game: new Game(this.state, socket), // TODO: look into why socket isn't emmitting when attached to state object
    };

    addEl(this.el, this.views.start.el, this.views.lobby.el, this.views.game.el);
    this.state.view = 'start';
  }

  update(change, current, previous) {
    console.log(`state changed: ${change}: ${previous} -> ${current}`);

    if (change === 'view') {
      if (previous) this.views[previous].hide();
      if (current) this.views[current].show();
    }
  }
}

const app = new App();
document.body.appendChild(app.el);
window.app = app;
