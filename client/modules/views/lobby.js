import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Lobby {
  constructor(state, socket) {
    this.state = state;
    this.socket = socket;
    this.socket.on('roomsUpdated', (data) => { this.updateRoomList(data); });

    autoBind(this);
    this.el = createEl('div', { className: 'view lobby hidden' });
    this.title = createEl('h1', { className: 'lobby-title', innerText: 'lobby view' });
    this.welcome = createEl('p', { className: 'lobby-welcome' });
    this.createRoomButton = createEl('button', { className: 'create-room-button', innerText: 'create a new room' }, {}, { click: this.createRoom });
    this.joinRoomLabel = createEl('label', { className: 'join-room-label', innerText: 'or join existing room:' });
    this.roomList = createEl('span', { className: 'room-list' });
    this.joinRoomButton = createEl('button', { className: 'join-room-button', innerText: 'Join' }, {}, { click: this.joinRoom });
    this.returnToStartButton = createEl('button', { className: 'return-button', innerText: 'Return to Start' }, {}, { click: this.returnToStart });
    addEl(this.el, this.title, this.welcome, this.createRoomButton, this.joinRoomLabel, this.roomList, this.joinRoomButton, this.returnToStartButton);
  }

  show() {
    this.el.classList.remove('hidden');
    this.welcome.innerText = `Welcome ${this.state.name}`;
    fetch('/getRooms').then(res => res.json()).then(this.updateRoomList);
  }

  hide() {
    this.el.classList.add('hidden');
  }

  createRoom() {
    fetch('/createRoom');
  }

  joinRoom() {
    const { value } = this.roomSelector;
    const { id } = this.socket;
    if (!value) {
      alert('select a room to join');
    } else {
      fetch(`/joinRoom/${id}/${value}`).then(res => res.json()).then(({ joined }) => {
        if (joined) {
          this.state.room = value;
          this.state.view = 'game';
        }
      });
    }
  }

  updateRoomList(rooms) {
    this.roomList.innerHTML = '';
    this.roomSelector = createEl('select', { className: 'room-list-selector', values: Object.keys(rooms) }, { multiple: true, size: Object.keys(rooms).length });
    addEl(this.roomList, this.roomSelector);
  }

  returnToStart() {
    this.state.name = null;
    this.state.view = 'start';
  }
}
