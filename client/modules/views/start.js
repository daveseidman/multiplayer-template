import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Start {
  constructor(state, socket) {
    this.state = state;
    this.socket = socket;
    autoBind(this);

    this.el = createEl('div', { className: 'view start hidden' });
    this.title = createEl('h1', { className: 'start-title', innerText: 'start view' });
    this.playerNameInput = createEl('input', { className: 'player-name', type: 'text', placeholder: 'your name' });
    this.playerNameSubmit = createEl('button', { className: 'player-submit', innerText: 'Submit' }, {}, { click: this.submitName });

    addEl(this.el, this.title, this.playerNameInput, this.playerNameSubmit);
  }

  show() {
    this.el.classList.remove('hidden');
    this.playerNameInput.value = '';
  }

  hide() {
    this.el.classList.add('hidden');
  }

  submitName() {
    const { id } = this.socket;
    const { value } = this.playerNameInput;
    if (value === '') alert('enter your name');
    else {
      this.state.name = value;
      fetch(`/playerName/${id}/${value}`).then(res => res.json()).then(({ nameSet }) => {
        if (nameSet) this.state.view = 'lobby';
      });
    }
  }
}
