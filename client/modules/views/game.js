import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Game {
  constructor(state, socket) {
    this.state = state;
    this.socket = socket;
    autoBind(this);

    this.el = createEl('div', { className: 'view game hidden' });
    this.title = createEl('h1', { className: 'game-title', innerText: 'game view' });
    this.welcome = createEl('p', { className: 'game-welcome' });
    this.playersLabel = createEl('label', { className: 'game-label', innerText: 'Players in this room' });
    this.playerList = createEl('div', { className: 'game-players' });
    this.upButton = createEl('button', { className: 'game-button', innerText: 'up' }, { direction: 'up' }, { click: () => { this.socket.emit('direction', 'up'); } });
    this.downButton = createEl('button', { className: 'game-button', innerText: 'down' }, { direction: 'down' }, { click: () => { this.socket.emit('direction', 'down'); } });
    this.leftButton = createEl('button', { className: 'game-button', innerText: 'left' }, { direction: 'left' }, { click: () => { this.socket.emit('direction', 'left'); } });
    this.rightButton = createEl('button', { className: 'game-button', innerText: 'right' }, { direction: 'right' }, { click: () => { this.socket.emit('direction', 'right'); } });
    this.moves = createEl('div', { className: 'game-moves' });
    this.returnToLobbyButton = createEl('button', { className: 'return-to-lobby-button', innerText: 'Return to Lobby' }, {}, { click: this.returnToLobby });

    this.socket.on('playersUpdated', this.updatePlayers);
    this.socket.on('direction', this.direction);

    addEl(this.el, this.title, this.welcome, this.playersLabel, this.playerList, this.upButton, this.downButton, this.leftButton, this.rightButton, this.moves, this.returnToLobbyButton);
  }

  show() {
    this.el.classList.remove('hidden');
    this.moves.innerHTML = '';
    this.welcome.innerText = `welcome to room ${this.state.room}`;
    fetch(`/getPlayers/${this.state.room}`).then(res => res.json()).then(this.updatePlayers);
  }

  hide() {
    this.el.classList.add('hidden');
  }

  updatePlayers(players) {
    this.playerList.innerHTML = '';
    players.forEach((player) => {
      addEl(this.playerList, createEl('p', { className: 'player-list-player', innerText: player.name }));
    });
  }

  returnToLobby() {
    fetch(`/leaveRoom/${this.socket.id}/${this.state.room}`).then(res => res.json()).then(({ left }) => {
      this.state.room = null;
      this.state.view = 'lobby';
    });
  }

  direction({ player, direction }) {
    const move = createEl('p', { className: 'game-moves-move', innerText: `${player} moved ${direction}` });
    addEl(this.moves, move);
  }
}
